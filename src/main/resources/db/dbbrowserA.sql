DROP TABLE IF EXISTS billionaires;

create sequence billionaires_S start 1 increment 1;

CREATE TABLE billionaires
(
    ID         int8         not null,
    first_name VARCHAR(250) NOT NULL,
    last_name  VARCHAR(250) NOT NULL,
    career     VARCHAR(250) DEFAULT NULL,
    primary key (ID)
);

GRANT ALL ON SEQUENCE billionaires_s TO dbbrowser;
GRANT ALL ON TABLE billionaires TO dbbrowser;

INSERT INTO billionaires (first_name, last_name, career)
VALUES ('Aliko', 'Dangote', 'Billionaire Industrialist'),
       ('Bill', 'Gates', 'Billionaire Tech Entrepreneur'),
       ('Folrunsho', 'Alakija', 'Billionaire Oil Magnate');