create sequence CONNECTION_S start 1 increment 1;

create table CONNECTION (
    ID int8 not null,
    NAME varchar(50),
    HOSTNAME varchar(50),
    PORT int8,
    DATABASE_NAME varchar(50),
    USERNAME varchar(50),
    PASSWORD varchar(50),
    primary key (ID);
)

alter table CONNECTION
    add constraint unique_name
    unique(NAME);