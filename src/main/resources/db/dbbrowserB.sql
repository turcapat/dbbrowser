DROP TABLE IF EXISTS cars;

create sequence cars_S start 1 increment 1;

CREATE TABLE cars
(
    ID     int8         not null,
    brand  VARCHAR(250) NOT NULL,
    model  VARCHAR(250) NOT NULL,
    wheels numeric(4, 0),
    primary key (ID)
);

GRANT ALL ON SEQUENCE cars_s TO dbbrowser;
GRANT ALL ON TABLE cars TO dbbrowser;

INSERT INTO cars (id, brand, model, wheels)
VALUES (nextval('cars_S'), 'Audi', 'A5', 4),
       (nextval('cars_S'), 'Mercedes', 'G', 6),
       (nextval('cars_S'), 'Scania', 'SC1', 8);

select *
from cars;