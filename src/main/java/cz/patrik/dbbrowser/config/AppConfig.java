package cz.patrik.dbbrowser.config;

import cz.patrik.dbbrowser.service.ConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
public class AppConfig  {

    @Value("${spring.datasource.url}")
    private String defaultDSurl;

    @Value("${spring.datasource.driver-class-name}")
    private String defaultDSdriverClassName;

    @Value("${spring.datasource.username}")
    private String defaultDSusername;

    @Value("${spring.datasource.password}")
    private String defaultDSpassword;

    @Autowired
    private ResolveConnectionFilter resolveConnectionFilter;

    /**
     * Register filter for parsing header with unique connection name
     * */
    @Bean
    public FilterRegistrationBean<ResolveConnectionFilter> filterRegistrationBean() {
        FilterRegistrationBean<ResolveConnectionFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(resolveConnectionFilter);
        registrationBean.addUrlPatterns("/api/metadata/*");
        registrationBean.addUrlPatterns("/api/statistics/*");
        return registrationBean;
    }


    /**
     * Abstract routing data source - handles multiple datasource connections
     */
    @Bean
    public AbstractRoutingDataSource dataSource() {
        AbstractRoutingDataSource abstractRoutingDataSource = new AbstractRoutingDataSource() {
            @Override
            protected Object determineCurrentLookupKey() {
                return ConnectionManager.getCurrentConnection();
            }
        };
        abstractRoutingDataSource.setTargetDataSources(ConnectionManager.getDataSourceConnections());
        abstractRoutingDataSource.setDefaultTargetDataSource(defaultDataSource());
        abstractRoutingDataSource.afterPropertiesSet();
        return abstractRoutingDataSource;
    }

    /**
     * Default data source
     */
    private DriverManagerDataSource defaultDataSource() {
        DriverManagerDataSource defaultDS = new DriverManagerDataSource();
        defaultDS.setUsername(defaultDSusername);
        defaultDS.setPassword(defaultDSpassword);
        defaultDS.setDriverClassName(defaultDSdriverClassName);
        defaultDS.setUrl(defaultDSurl);
        return defaultDS;
    }
}