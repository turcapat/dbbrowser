package cz.patrik.dbbrowser.config;

import cz.patrik.dbbrowser.service.ConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static cz.patrik.dbbrowser.model.Headers.X_CONNECTION_NAME;

@Component
public class ResolveConnectionFilter extends OncePerRequestFilter {

    @Autowired
    private ConnectionManager connectionManager;

    /**
     * Look for mandatory header (X-Connection-Name) - unique identifier of the connection
     * Set this connection as current connection to work with
     * */
    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        String connectionId = httpServletRequest.getHeader(X_CONNECTION_NAME);
        if(StringUtils.isEmpty(connectionId)){
            throw new IllegalStateException("No database connection selected, header X-Connection-Name is not present.");
        }
        connectionManager.setCurrentConnection(connectionId);
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}
