package cz.patrik.dbbrowser.utils;

import cz.patrik.dbbrowser.model.Connection;

public class ConnectionUtils {

    public static String createUri(Connection c, String databasePrefix){
        StringBuilder sb = new StringBuilder();
        return sb.append(databasePrefix)
                .append(c.getHostname())
                .append(":")
                .append(c.getPort())
                .append("/")
                .append(c.getDatabaseName())
                .toString();
    }
}
