package cz.patrik.dbbrowser.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);

    @ExceptionHandler(value = {IllegalStateException.class})
    protected ResponseEntity<Object> handleException(IllegalStateException ex) {
        logger.error("Unexpected error", ex);
        throw new IllegalStateException("Unexpected error", ex);
    }

    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<Object> handleException(Exception ex) {
        logger.error("Unexpected error", ex);
        throw new IllegalStateException("Unexpected error", ex);
    }
}

