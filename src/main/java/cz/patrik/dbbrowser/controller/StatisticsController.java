package cz.patrik.dbbrowser.controller;

import cz.patrik.dbbrowser.model.ColumnDetails;
import cz.patrik.dbbrowser.model.TableDetails;
import cz.patrik.dbbrowser.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/statistics")
public class StatisticsController {

    @Autowired
    private StatisticsService statisticsService;

    @GetMapping
    public List<TableDetails> getTableStatistics(@RequestParam("tablename") String tableName) {
        return statisticsService.getTableStatistics(tableName);
    }

    @GetMapping(value = "/{tableId}")
    public List<ColumnDetails> getColumnStatistics(@PathVariable("tableId") String tableName, @RequestParam("columnName") String columnName) {
        return statisticsService.getColumnStatistics(tableName, columnName);
    }

}

