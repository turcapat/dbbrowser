package cz.patrik.dbbrowser.controller;


import cz.patrik.dbbrowser.model.Connection;
import cz.patrik.dbbrowser.service.ConnectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/connection")
public class ConnectionController {

    @Autowired
    private ConnectionService connectionService;

    @GetMapping
    public List<Connection> getAllConnections() {
        return connectionService.getAllConnections();
    }

    @PostMapping
    public Connection createConnection(@RequestBody Connection connection) {
        return connectionService.saveConnection(connection);
    }

    @PutMapping(value = "/{connectionId}")
    public Connection updateConnection(@RequestBody Connection connection,@PathVariable(value = "connectionId") String connectionName) {
        return connectionService.updateConnection(connection, connectionName);
    }

    @DeleteMapping(value = "/{connectionId}")
    public void deleteConnection(@PathVariable(value = "connectionId") String connectionName) {
        connectionService.deleteConnection(connectionName);
    }

}

