package cz.patrik.dbbrowser.controller;

import cz.patrik.dbbrowser.model.Column;
import cz.patrik.dbbrowser.model.Schema;
import cz.patrik.dbbrowser.model.Table;
import cz.patrik.dbbrowser.service.MetadataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(value = "/api/metadata")
public class MetadataController {

    @Autowired
    private MetadataService metadataService;

    @GetMapping(value = "/schemas")
    public List<Schema> getTableSchemas() {
        return metadataService.getSchemas();
    }

    @GetMapping(value = "/tables")
    public List<Table> getAllTables(@RequestParam(value = "schema", required = false) String schema,
                                    @RequestParam(value = "catalog", required = false) String catalog) {
        return metadataService.getTables(catalog,schema);
    }

    @GetMapping(value = "/tables/{tableId}/columns")
    public List<Column> getTableColumns(@PathVariable("tableId") String tableId) {
        return metadataService.getTableColumns(tableId);
    }

    @GetMapping(value = "/tables/{tableId}/preview")
    public List<HashMap<String, Object>> getDataPreview(@PathVariable("tableId") String tableId) {
        return metadataService.getDataPreview(tableId);
    }

}
