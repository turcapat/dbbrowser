package cz.patrik.dbbrowser.model;

public class Table {
    private String tableCatalog;
    private String tableSchema;
    private String tableName;
    private String tableType;
    private String explanatoryComment;
    private String typesCatalog;
    private String typesSchema;
    private String typeName;
    private String selfReferencingColumnName;
    private String referenceGeneration;

    public String getTableCatalog() {
        return tableCatalog;
    }

    public void setTableCatalog(String tableCatalog) {
        this.tableCatalog = tableCatalog;
    }

    public String getTableSchema() {
        return tableSchema;
    }

    public void setTableSchema(String tableSchema) {
        this.tableSchema = tableSchema;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableType() {
        return tableType;
    }

    public void setTableType(String tableType) {
        this.tableType = tableType;
    }

    public String getExplanatoryComment() {
        return explanatoryComment;
    }

    public void setExplanatoryComment(String explanatoryComment) {
        this.explanatoryComment = explanatoryComment;
    }

    public String getTypesCatalog() {
        return typesCatalog;
    }

    public void setTypesCatalog(String typesCatalog) {
        this.typesCatalog = typesCatalog;
    }

    public String getTypesSchema() {
        return typesSchema;
    }

    public void setTypesSchema(String typesSchema) {
        this.typesSchema = typesSchema;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getSelfReferencingColumnName() {
        return selfReferencingColumnName;
    }

    public void setSelfReferencingColumnName(String selfReferencingColumnName) {
        this.selfReferencingColumnName = selfReferencingColumnName;
    }

    public String getReferenceGeneration() {
        return referenceGeneration;
    }

    public void setReferenceGeneration(String referenceGeneration) {
        this.referenceGeneration = referenceGeneration;
    }
}
