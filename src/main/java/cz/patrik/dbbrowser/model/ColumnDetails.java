package cz.patrik.dbbrowser.model;

public class ColumnDetails {
    private Object min;
    private Object max;
    private Object avg;
    private Object median;

    public Object getMin() {
        return min;
    }

    public void setMin(Object min) {
        this.min = min;
    }

    public Object getMax() {
        return max;
    }

    public void setMax(Object max) {
        this.max = max;
    }

    public Object getAvg() {
        return avg;
    }

    public void setAvg(Object avg) {
        this.avg = avg;
    }

    public Object getMedian() {
        return median;
    }

    public void setMedian(Object median) {
        this.median = median;
    }
}
