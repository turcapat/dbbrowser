package cz.patrik.dbbrowser.model;

import javax.persistence.*;
import javax.persistence.Column;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "CONNECTION")
public class Connection {

    @Id
    @Column(name = "ID")
    @SequenceGenerator(name= "ConnectionSequenceGenerator", sequenceName = "CONNECTION_S")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ConnectionSequenceGenerator")
    private Long id;

    @Column(name = "NAME", unique = true)
    private String name;

    @Column(name = "HOSTNAME")
    private String hostname;

    @Column(name = "PORT")
    private int port;

    @Column(name = "DATABASE_NAME")
    private String databaseName;

    @Column(name = "USERNAME")
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Connection that = (Connection) o;
        return port == that.port &&
                Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(hostname, that.hostname) &&
                Objects.equals(databaseName, that.databaseName) &&
                Objects.equals(username, that.username) &&
                Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, hostname, port, databaseName, username, password);
    }
}
