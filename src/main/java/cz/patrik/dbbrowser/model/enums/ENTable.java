package cz.patrik.dbbrowser.model.enums;

public enum ENTable {
    TABLE_CAT,
    TABLE_SCHEM,
    TABLE_NAME,
    TABLE_TYPE,
    REMARKS,
    TYPE_CAT,
    TYPE_SCHEM,
    TYPE_NAME,
    SELF_REFERENCING_COL_NAME,
    REF_GENERATION;
}
