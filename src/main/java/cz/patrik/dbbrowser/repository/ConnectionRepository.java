package cz.patrik.dbbrowser.repository;

import cz.patrik.dbbrowser.model.Connection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConnectionRepository extends JpaRepository<Connection, Long> {

    public Connection findByName(String connectionName);
}
