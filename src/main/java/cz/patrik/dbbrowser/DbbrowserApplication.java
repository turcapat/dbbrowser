package cz.patrik.dbbrowser;

import cz.patrik.dbbrowser.model.Connection;
import cz.patrik.dbbrowser.repository.ConnectionRepository;
import cz.patrik.dbbrowser.service.ConnectionManager;
import cz.patrik.dbbrowser.utils.ConnectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.sql.SQLException;
import java.util.List;

@SpringBootApplication
public class DbbrowserApplication {
    private static final Logger logger = LoggerFactory.getLogger(DbbrowserApplication.class);

    @Autowired
    private ConnectionManager connectionManager;

    @Autowired
    private ConnectionRepository connectionRepository;

    /**
     *  Load all data sources to local memory
     * */
    @EventListener(ApplicationReadyEvent.class)
    public void onApplicationEvent() {
        logger.info("Initializing connections.");
        initializeDataSourceConnections();
        logger.info("All existing connections initialized.");
    }

    private void initializeDataSourceConnections() {
        List<Connection> allConnections = connectionRepository.findAll();
        allConnections.forEach(c -> {
            connectionManager.addDataSource(c);
        });
    }

    public static void main(String[] args) {
        SpringApplication.run(DbbrowserApplication.class, args);
    }
}
