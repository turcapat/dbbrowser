package cz.patrik.dbbrowser.service;

import cz.patrik.dbbrowser.model.Column;
import cz.patrik.dbbrowser.model.Schema;
import cz.patrik.dbbrowser.model.Table;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

@Service
public class MetadataService {
    private static final String PATTERN_ALL = "%";
    private static final String SELECT_DATA_PREVIEW = "SELECT * from %s fetch first ? rows only";

    @Autowired
    private ResultMapper resultMapper;

    @Autowired
    private ConnectionManager connectionManager;

    @Value("${data.preview.count}")
    private Integer dataPreviewCount;

    /**
     * Get all available tables for actual connection
     * @param catalog - optional
     * @param schema - optional
     * */
    public List<Table> getTables(String catalog, String schema) {
        try {
            ResultSet rs = getMetadata().getTables(
                    StringUtils.isEmpty(catalog) ? null : catalog,
                    StringUtils.isEmpty(schema) ? null : schema,
                    PATTERN_ALL,
                    null);
            List<Table> tableNames = new ArrayList<>();
            while (rs.next()) {
                tableNames.add(resultMapper.mapToTable(rs));
            }
            return tableNames;
        } catch (SQLException e) {
            throw new IllegalStateException("Unable to get tables - maybe table does not exist in this connection ?", e);
        }
    }

    /**
     * Get all available columns for given table
     * @param tableName - mandatory
     * */
    public List<Column> getTableColumns(String tableName) {
        try {
            ResultSet rsColumns = getMetadata().getColumns(null, PATTERN_ALL, tableName, PATTERN_ALL);
            ResultSet rsPrimaryKeys = getMetadata().getPrimaryKeys(null, null, tableName);
            HashSet<String> primaryKeys = resultMapper.createSetFromResultSet(rsPrimaryKeys);
            ArrayList<Column> columnNames = new ArrayList<>();
            while (rsColumns.next()) {
                columnNames.add(resultMapper.mapToColumn(rsColumns, primaryKeys));
            }
            return columnNames;
        } catch (SQLException e) {
            throw new IllegalStateException("Unable to get columns - maybe table does not exist in this connection ?", e);
        }
    }

    /**
     * Get all available schemas for actual connection
     * */
    public List<Schema> getSchemas() {
        try {
            ResultSet rs = getMetadata().getSchemas(null, PATTERN_ALL);
            ArrayList<Schema> schemaNames = new ArrayList<>();
            while (rs.next()) {
                schemaNames.add(resultMapper.mapToSchema(rs));
            }
            return schemaNames;
        } catch (SQLException e) {
            throw new IllegalStateException("Unable to get schemas.", e);
        }
    }

    /**
     * Get data preview for given table. Result size configurable by property data.preview.count
     * @param tableName - mandatory
     * */
    public List<HashMap<String, Object>> getDataPreview(String tableName) {
        java.sql.Connection actualConnection = connectionManager.getActualConnection();
        try {
            PreparedStatement stmt = actualConnection.prepareStatement(String.format(SELECT_DATA_PREVIEW, tableName));
            stmt.setInt(1, dataPreviewCount);
            ResultSet rs = stmt.executeQuery();
            return resultMapper.createListFromResultSet(rs);
        } catch (SQLException e) {
            throw new IllegalStateException("Unable to get data preview - maybe table does not exist in this connection ?", e);
        }
    }

    private DatabaseMetaData getMetadata() {
        try {
            return connectionManager.getActualConnection().getMetaData();
        } catch (SQLException e) {
            throw new IllegalStateException("Unable to parse actual connection informations", e);
        }
    }

}
