package cz.patrik.dbbrowser.service;

import cz.patrik.dbbrowser.model.Connection;
import cz.patrik.dbbrowser.repository.ConnectionRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ConnectionService {

    @Autowired
    private ConnectionRepository connectionRepository;

    @Autowired
    private ConnectionManager connectionManager;

    /**
     * Get all available connections
     */
    public List<Connection> getAllConnections() {
        return connectionRepository.findAll();
    }

    /**
     * Create new connection, make it available to use immediately
     *
     * @param c - new connection object
     */
    public Connection saveConnection(Connection c) {
        connectionManager.addDataSource(c);
        return connectionRepository.save(c);
    }

    /**
     * Modify connection
     *
     * @param con - updated values
     */
    public Connection updateConnection(Connection con, String connectionName) {
        Connection connection = connectionRepository.findByName(connectionName);
        connectionManager.updateConnection(con, connectionName);
        BeanUtils.copyProperties(con, connection);
        return connectionRepository.save(connection);
    }

    /**
     * Delete connection
     *
     */
    public void deleteConnection(String connectionName) {
        Connection connection = connectionRepository.findByName(connectionName);
        connectionManager.removeDataSource(connection.getName());
        connectionRepository.delete(connection);
    }
}
