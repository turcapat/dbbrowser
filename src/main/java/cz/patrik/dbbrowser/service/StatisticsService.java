package cz.patrik.dbbrowser.service;

import cz.patrik.dbbrowser.model.ColumnDetails;
import cz.patrik.dbbrowser.model.TableDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.List;

/**
 *  TODO implement
 * */
@Service
public class StatisticsService {
    private static final Logger logger = LoggerFactory.getLogger(StatisticsService.class);
    private static final String PATTERN_ALL = "%";
    private static final String SELECT_DATA_PREVIEW = "SELECT COUNT(*) FROM %s";

    @Autowired
    private ConnectionManager connectionManager;

    public int getTableColumnCount(String tableId) {
        try {
            ResultSet rsColumns = getMetadata().getColumns(null, PATTERN_ALL, tableId, PATTERN_ALL);
            return rsColumns.getMetaData().getColumnCount();
        } catch (SQLException e) {
            logger.error("Unable to get column count.", e);
            throw new IllegalStateException("Unable to get column count.", e);
        }
    }

    public int getTableRowCount(String tableName) {
        java.sql.Connection actualConnection = connectionManager.getActualConnection();
        try {
            PreparedStatement stmt = actualConnection.prepareStatement(String.format(SELECT_DATA_PREVIEW, tableName));
            ResultSet rs = stmt.executeQuery();
            return 1;
        } catch (SQLException e) {
            logger.error("Unable to get row count.", e);
            throw new IllegalStateException("Unable to get row count.", e);
        }
    }

    private DatabaseMetaData getMetadata() {
        Connection actualConnection = connectionManager.getActualConnection();
        try {
            return actualConnection.getMetaData();
        } catch (SQLException e) {
            logger.error("Unable to parse actual connection informations.", e);
            throw new IllegalStateException("Unable to parse actual connection informations", e);
        }
    }

    public List<ColumnDetails> getColumnStatistics(String tableName, String columnName) {
        // TODO implement
        return null;
    }

    public List<TableDetails> getTableStatistics(String tableName) {
        // TODO implement
        return null;
    }
}
