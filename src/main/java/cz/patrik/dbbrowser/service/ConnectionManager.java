package cz.patrik.dbbrowser.service;

import cz.patrik.dbbrowser.utils.ConnectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Configuration
public class ConnectionManager {

    @Autowired
    private AbstractRoutingDataSource abstractRoutingDataSource;

    @Value("${spring.datasource.driver-class-name}")
    private String defaultDSdriverClassName;

    @Value("${spring.datasource.url.prefix}")
    private String datasourcUrlPrefix;

    private static Map<Object, Object> dataSourceConnections = new ConcurrentHashMap<>();
    private static ThreadLocal<String> currentConnection = new ThreadLocal<>();

    /**
     *  Return all stored connections
     * */
    public static Map<Object, Object> getDataSourceConnections() {
        return dataSourceConnections;
    }

    /**
     *  Return name of the current connection
     * */
    public static ThreadLocal<String> getCurrentConnection() {
        return currentConnection;
    }

    /**
     * Creates new data source, make it available to use immediately
     */
    private void addDataSource(String connectionName, String url, String username, String password) throws SQLException {
        DataSource dataSource = DataSourceBuilder.create()
                .driverClassName(defaultDSdriverClassName)
                .url(url)
                .username(username)
                .password(password)
                .build();

        try (Connection connection = dataSource.getConnection()) {
            dataSourceConnections.put(connectionName, dataSource);
            abstractRoutingDataSource.afterPropertiesSet();
        }
    }

    /**
     * Set current connection
     *
     * @param connectionId - mandatory unique name of the connection
     */
    public void setCurrentConnection(String connectionId) {
        if (dataSourceConnections.containsKey(connectionId)) {
            currentConnection.set(connectionId);
        } else {
            throw new IllegalStateException("Connection does not exist yet, you have to create one first !");
        }
    }

    /**
     * Get actual connection to work with
     */
    public Connection getActualConnection() {
        DataSource dataSource = (DataSource) Optional.of(dataSourceConnections.get(currentConnection.get()))
                .orElseThrow(() -> new IllegalStateException("Unable to obtain data source."));
        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new IllegalStateException("Unable to obtain actual connection.", e);
        }
    }

    public void updateConnection(cz.patrik.dbbrowser.model.Connection connection, String oldConnectionName) {
        if (dataSourceConnections.containsKey(oldConnectionName)) {
            try {
                dataSourceConnections.remove(oldConnectionName);
                addDataSource(connection.getName(), ConnectionUtils.createUri(connection, datasourcUrlPrefix), connection.getUsername(), connection.getPassword());
            } catch (SQLException e) {
                throw new IllegalStateException("Unable to update connection.", e);
            }
        } else {
            throw new IllegalStateException("Provided connection oes not exist.");
        }
    }

    public void removeDataSource(String name) {
        dataSourceConnections.remove(name);
        abstractRoutingDataSource.afterPropertiesSet();
    }

    public void addDataSource(cz.patrik.dbbrowser.model.Connection c) {
        try {
            addDataSource(c.getName(), ConnectionUtils.createUri(c, datasourcUrlPrefix), c.getUsername(), c.getPassword());
        } catch (SQLException e) {
            throw new IllegalStateException("Unable to add new connection.", e);
        }
    }
}

