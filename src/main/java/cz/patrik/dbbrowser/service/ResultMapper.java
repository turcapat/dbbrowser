package cz.patrik.dbbrowser.service;

import cz.patrik.dbbrowser.model.Column;
import cz.patrik.dbbrowser.model.Schema;
import cz.patrik.dbbrowser.model.Table;
import cz.patrik.dbbrowser.model.enums.ENColumn;
import cz.patrik.dbbrowser.model.enums.ENSchema;
import cz.patrik.dbbrowser.model.enums.ENTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

@Component
public class ResultMapper {
    private static final Logger logger = LoggerFactory.getLogger(ResultMapper.class);

    private static final String COLUMN_NAME = "COLUMN_NAME";

    protected Schema mapToSchema(ResultSet rs) throws SQLException {
        Schema schema = new Schema();
        schema.setTableSchema(rs.getString(ENSchema.TABLE_SCHEM.toString()));
        schema.setTableCatalog(rs.getString(ENSchema.TABLE_CATALOG.toString()));
        return schema;
    }

    protected Column mapToColumn(ResultSet rs, HashSet<String> primaryKeys) throws SQLException {
        Column column = new Column();
        column.setTableCatalog(rs.getString(ENColumn.TABLE_CAT.toString()));
        column.setTableSchema(rs.getString(ENColumn.TABLE_SCHEM.toString()));
        column.setTableName(rs.getString(ENColumn.TABLE_NAME.toString()));
        column.setColumnName(rs.getString(ENColumn.COLUMN_NAME.toString()));
        column.setSQLtype(rs.getInt(ENColumn.SQL_DATA_TYPE.toString()));
        column.setTypeName(rs.getString(ENColumn.TYPE_NAME.toString()));
        column.setColumnSize(rs.getInt(ENColumn.COLUMN_SIZE.toString()));
        column.setFractionalDigits(rs.getInt(ENColumn.DECIMAL_DIGITS.toString()));
        column.setRadix(rs.getInt(ENColumn.NUM_PREC_RADIX.toString()));
        column.setNullable(rs.getInt(ENColumn.NULLABLE.toString()));
        column.setRemarks(rs.getString(ENColumn.REMARKS.toString()));
        column.setDefaultValue(rs.getString(ENColumn.COLUMN_DEF.toString()));
        column.setCharOctetLength(rs.getInt(ENColumn.CHAR_OCTET_LENGTH.toString()));
        column.setOrdinalPosition(rs.getInt(ENColumn.ORDINAL_POSITION.toString()));
        column.setIsNullable(rs.getString(ENColumn.IS_NULLABLE.toString()));
        column.setScopeCatalog(rs.getString(ENColumn.SCOPE_CATALOG.toString()));
        column.setScopeSchema(rs.getString(ENColumn.SCOPE_SCHEMA.toString()));
        column.setScopeTable(rs.getString(ENColumn.SCOPE_TABLE.toString()));
        column.setSourceDataType(rs.getShort(ENColumn.SOURCE_DATA_TYPE.toString()));
        column.setIsAutoIncrement(rs.getString(ENColumn.IS_AUTOINCREMENT.toString()));
        column.setIsGeneratedColumn(rs.getString(ENColumn.IS_GENERATEDCOLUMN.toString()));
        column.setIsGeneratedColumn(rs.getString(ENColumn.IS_GENERATEDCOLUMN.toString()));
        if (!CollectionUtils.isEmpty(primaryKeys)) {
            column.setPrimaryKey(primaryKeys.contains(rs.getString(ENColumn.COLUMN_NAME.toString())));
        }
        return column;
    }

    protected Table mapToTable(ResultSet rs) throws SQLException {
        Table table = new Table();
        table.setTableCatalog(rs.getString(ENTable.TABLE_CAT.toString()));
        table.setTableSchema(rs.getString(ENTable.TABLE_SCHEM.toString()));
        table.setTableName(rs.getString(ENTable.TABLE_NAME.toString()));
        table.setTableType(rs.getString(ENTable.TABLE_TYPE.toString()));
        table.setExplanatoryComment(rs.getString(ENTable.REMARKS.toString()));
        table.setTypesCatalog(rs.getString(ENTable.TYPE_CAT.toString()));
        table.setTypesSchema(rs.getString(ENTable.TYPE_SCHEM.toString()));
        table.setTypeName(rs.getString(ENTable.TYPE_NAME.toString()));
        table.setSelfReferencingColumnName(rs.getString(ENTable.SELF_REFERENCING_COL_NAME.toString()));
        table.setReferenceGeneration(rs.getString(ENTable.REF_GENERATION.toString()));
        return table;
    }

    protected List<HashMap<String, Object>> createListFromResultSet(ResultSet rs) throws SQLException {
        ResultSetMetaData md = rs.getMetaData();
        List<HashMap<String, Object>> result = new ArrayList<>();
        int columns = md.getColumnCount();

        while (rs.next()) {
            HashMap<String, Object> row = new HashMap<>(columns);
            for (int i = 1; i <= columns; ++i) {
                row.put(md.getColumnName(i), rs.getObject(i));
            }
            result.add(row);
        }
        return result;
    }

    protected HashSet<String> createSetFromResultSet(ResultSet rs) {
        HashSet<String> pks = new HashSet<>();
        try {
            while (rs.next()) {
                pks.add(rs.getString(COLUMN_NAME));
            }
            return pks;
        } catch (SQLException e) {
            logger.warn("Unable to process primaryKeys."); // not a show stopper
        }
        return null;
    }
}
