package cz.patrik.dbbrowser;


import cz.patrik.dbbrowser.config.ResolveConnectionFilter;
import cz.patrik.dbbrowser.controller.MetadataController;
import cz.patrik.dbbrowser.model.Headers;
import cz.patrik.dbbrowser.repository.ConnectionRepository;
import cz.patrik.dbbrowser.service.ConnectionManager;
import cz.patrik.dbbrowser.service.MetadataService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class RequestFilterTest {
    private static final String CONNECTION_A = "ConnA";
    private static final String CONNECTION_B = "ConnB";

    @Autowired
    private WebApplicationContext wac;

    private MockMvc mockMvc;

    @Mock
    private MetadataService metadataService;

    @Mock
    private ConnectionManager connectionManager;

    @Autowired
    @InjectMocks
    private ResolveConnectionFilter resolveConnectionFilter;

    @Autowired
    @InjectMocks
    private MetadataController metadataController;

    @Before
    public void before() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)
                .addFilters(resolveConnectionFilter)
                .build();
    }

    @Test
    public void selectConnectionA() throws Exception {
        when(metadataService.getSchemas()).thenReturn(new ArrayList<>());
        this.mockMvc.perform(get("/api/metadata/schemas").headers(getHeaders(CONNECTION_A))).andExpect(status().isOk());
        verify(connectionManager, times(1)).setCurrentConnection(eq(CONNECTION_A));
        verify(connectionManager, times(0)).setCurrentConnection(eq(CONNECTION_B));
    }

    @Test
    public void selectConnectionB() throws Exception {
        this.mockMvc.perform(get("/api/metadata/schemas").headers(getHeaders(CONNECTION_B))).andExpect(status().isOk());
        verify(connectionManager, times(0)).setCurrentConnection(eq(CONNECTION_A));
        verify(connectionManager, times(1)).setCurrentConnection(eq(CONNECTION_B));
    }

    @Test(expected = IllegalStateException.class)
    public void withoutConnection() throws Exception {
        this.mockMvc.perform(get("/api/connection")).andExpect(status().isOk());
    }

    private HttpHeaders getHeaders(String connectionValue) {
        HttpHeaders headers = new HttpHeaders();
        headers.set(Headers.X_CONNECTION_NAME, connectionValue);
        return headers;
    }


    @Configuration
    @PropertySource("classpath:/test.properties")
    @ComponentScan(basePackages = {"cz.patrik.dbbrowser.service", "cz.patrik.dbbrowser.config", "cz.patrik.dbbrowser.controller"})
    public static class TestConfig {

        @MockBean
        public ConnectionRepository connectionRepository;

        @MockBean
        public MetadataService metadataService;

        @Bean
        public ResolveConnectionFilter resolveConnectionFilter() {
            return new ResolveConnectionFilter();
        }

        @Bean
        public MetadataController metadataController() {
            return new MetadataController();
        }
    }
}
