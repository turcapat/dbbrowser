package cz.patrik.dbbrowser;

import cz.patrik.dbbrowser.model.Connection;
import cz.patrik.dbbrowser.repository.ConnectionRepository;
import cz.patrik.dbbrowser.service.ConnectionManager;
import cz.patrik.dbbrowser.service.ConnectionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


@RunWith(MockitoJUnitRunner.class)
public class ConnectionServiceTest {

    @Mock
    private ConnectionManager connectionManager;

    @Mock
    private ConnectionRepository connectionRepository;

    @InjectMocks
    private ConnectionService connectionService = new ConnectionService();

    @Test
    public void getConnectionsTest() {
        ArrayList<Connection> cons = new ArrayList<>();
        Connection c = new Connection();
        c.setName("Con Name");
        c.setDatabaseName("DB1");
        cons.add(c);
        Mockito.when(connectionRepository.findAll()).thenReturn(cons);

        List<Connection> connections = connectionService.getAllConnections();
        verify(connectionRepository, times(1)).findAll();
        assertEquals(1, connections.size());
        assertEquals("Con Name", connections.get(0).getName());
        assertEquals("DB1", connections.get(0).getDatabaseName());
    }

    @Test
    public void createConnectionsTest() {
        Connection c = new Connection();
        c.setName("Con Name");
        c.setDatabaseName("DB1");
        Mockito.when(connectionRepository.save(any())).thenReturn(c);

        Connection connection = connectionService.saveConnection(c);
        verify(connectionRepository, times(1)).save(c);
        verify(connectionManager, times(1)).addDataSource(c);
        assertEquals("Con Name", connection.getName());
        assertEquals("DB1", connection.getDatabaseName());
    }

    @Test
    public void updateConnectionsTest() {
        Connection c = new Connection();
        c.setName("Con Name");
        c.setDatabaseName("DB1");

        Connection c2 = new Connection();
        c2.setName("Con2 Name");
        c2.setDatabaseName("DB2");

        Mockito.when(connectionRepository.findByName(any())).thenReturn(c);
        Mockito.when(connectionRepository.save(any())).thenReturn(c2);

        Connection connection = connectionService.updateConnection(c, c.getName());
        verify(connectionRepository, times(1)).findByName(c.getName());
        verify(connectionRepository, times(1)).save(c);
        verify(connectionManager, times(1)).updateConnection(c, c.getName());
        assertEquals("Con2 Name", connection.getName());
        assertEquals("DB2", connection.getDatabaseName());
    }

    @Test
    public void deleteConnectionsTest() {
        Connection c = new Connection();
        c.setName("Con Name");
        c.setDatabaseName("DB1");
        Mockito.when(connectionRepository.findByName(c.getName())).thenReturn(c);

        connectionService.deleteConnection(c.getName());
        verify(connectionRepository, times(1)).delete(c);
        verify(connectionManager, times(1)).removeDataSource(c.getName());
    }
}

