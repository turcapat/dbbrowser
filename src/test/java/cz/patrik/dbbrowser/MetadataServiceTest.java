package cz.patrik.dbbrowser;

import cz.patrik.dbbrowser.model.Column;
import cz.patrik.dbbrowser.model.Schema;
import cz.patrik.dbbrowser.model.Table;
import cz.patrik.dbbrowser.model.enums.ENColumn;
import cz.patrik.dbbrowser.model.enums.ENSchema;
import cz.patrik.dbbrowser.model.enums.ENTable;
import cz.patrik.dbbrowser.repository.ConnectionRepository;
import cz.patrik.dbbrowser.service.ConnectionManager;
import cz.patrik.dbbrowser.service.MetadataService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;

import javax.sql.DataSource;
import java.sql.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class MetadataServiceTest {

    private static final String CONNECTION_A = "ConnA";

    @Autowired
    private MetadataService metadataService;

    @MockBean
    public AbstractRoutingDataSource abstractRoutingDataSource;

    @Autowired
    @InjectMocks
    private ConnectionManager connectionManager;

    @Value("${spring.datasource.driver-class-name}")
    private String defaultDSdriverClassName;

    private ResultSet resultSetMock;
    private DatabaseMetaData metaData;
    private Connection con;

    @Before
    public void before() {
        metaData = mock(DatabaseMetaData.class);
        resultSetMock = Mockito.mock(ResultSet.class);

        try {
            con = mock(Connection.class);
            when(con.getMetaData()).thenReturn(metaData);
            DataSource ds = mock(DataSource.class);
            when(ds.getConnection()).thenReturn(con);

            Map<Object, Object> dataSourceConnections = new ConcurrentHashMap<>();
            dataSourceConnections.put(CONNECTION_A, ds);
            ReflectionTestUtils.setField(connectionManager, "dataSourceConnections", dataSourceConnections);

            ThreadLocal<String> actualConn = new ThreadLocal<>();
            actualConn.set(CONNECTION_A);
            ReflectionTestUtils.setField(connectionManager, "currentConnection", actualConn);

        } catch (SQLException e) {
            throw new IllegalStateException("unable to initialize tests.");
        }
    }

    @Test
    public void getSchemasTest() throws Exception {
        String tableCatalog = "Table catalog";
        String tableSchema = "Table schema";

        Mockito.when(resultSetMock.getString(ENSchema.TABLE_SCHEM.toString())).thenReturn(tableSchema);
        Mockito.when(resultSetMock.getString(ENSchema.TABLE_CATALOG.toString())).thenReturn(tableCatalog);

        when(metaData.getSchemas(any(), any())).thenReturn(resultSetMock);
        when(resultSetMock.next()).thenReturn(true).thenReturn(false);

        List<Schema> schemas = metadataService.getSchemas();
        assertEquals(1, schemas.size());
        assertEquals(tableCatalog, schemas.get(0).getTableCatalog());
        assertEquals(tableSchema, schemas.get(0).getTableSchema());
    }

    @Test
    public void getTablesTest() throws Exception {
        String tableCatalog1 = "Table cat 1";
        String tableSchema1 = "Table schema 1";
        String tableName1 = "Table name 1";

        String tableCatalog2 = "Table cat 2";
        String tableSchema2 = "Table schema 2";
        String tableName2 = "Table name 2";

        Mockito.when(resultSetMock.getString(ENTable.TABLE_CAT.toString())).thenReturn(tableCatalog1).thenReturn(tableCatalog2);
        Mockito.when(resultSetMock.getString(ENTable.TABLE_SCHEM.toString())).thenReturn(tableSchema1).thenReturn(tableSchema2);
        Mockito.when(resultSetMock.getString(ENTable.TABLE_NAME.toString())).thenReturn(tableName1).thenReturn(tableName2);

        when(metaData.getTables(any(), any(), any(), any())).thenReturn(resultSetMock);
        when(resultSetMock.next()).thenReturn(true).thenReturn(true).thenReturn(false);

        List<Table> tables = metadataService.getTables(null, null);
        assertEquals(2, tables.size());
        assertEquals(tableCatalog1, tables.get(0).getTableCatalog());
        assertEquals(tableSchema1, tables.get(0).getTableSchema());
        assertEquals(tableName1, tables.get(0).getTableName());
        assertEquals(tableSchema2, tables.get(1).getTableSchema());
        assertEquals(tableCatalog2, tables.get(1).getTableCatalog());
        assertEquals(tableName2, tables.get(1).getTableName());
    }

    @Test
    public void getTableColumnsTest() throws Exception {
        String tableCatalog1 = "Table cat 1";
        String tableSchema1 = "Table schema 1";
        String tableName1 = "Table name 1";
        String columnName1 = "Column name 1";

        Mockito.when(resultSetMock.getString(ENColumn.TABLE_CAT.toString())).thenReturn(tableCatalog1);
        Mockito.when(resultSetMock.getString(ENColumn.TABLE_SCHEM.toString())).thenReturn(tableSchema1);
        Mockito.when(resultSetMock.getString(ENColumn.TABLE_NAME.toString())).thenReturn(tableName1);
        Mockito.when(resultSetMock.getString(ENColumn.COLUMN_NAME.toString())).thenReturn(columnName1);
        when(metaData.getColumns(any(), any(), eq(tableName1), any())).thenReturn(resultSetMock);
        when(resultSetMock.next()).thenReturn(true).thenReturn(false);

        ResultSet resultSetMock2 = Mockito.mock(ResultSet.class, "rsPrimaryKeys");
        Mockito.when(resultSetMock2.getString(ENColumn.COLUMN_NAME.toString())).thenReturn(columnName1);
        when(resultSetMock2.next()).thenReturn(true).thenReturn(false);
        when(metaData.getPrimaryKeys(any(), any(), eq(tableName1))).thenReturn(resultSetMock2);

        List<Column> columns = metadataService.getTableColumns(tableName1);
        assertEquals(1, columns.size());
        assertEquals(tableCatalog1, columns.get(0).getTableCatalog());
        assertEquals(tableSchema1, columns.get(0).getTableSchema());
        assertEquals(tableName1, columns.get(0).getTableName());
        assertTrue(columns.get(0).isPrimaryKey());
    }

    @Test
    public void getDataPreview() throws Exception {
        String tableName1 = "Table name 1";
        String columnName1 = "Column name 1";
        String columnValue = "Column value";

        PreparedStatement stmt = mock(PreparedStatement.class);
        Mockito.when(con.prepareStatement(any(String.class))).thenReturn(stmt);
        Mockito.when(stmt.executeQuery()).thenReturn(resultSetMock);

        ResultSetMetaData md = mock(ResultSetMetaData.class);
        Mockito.when(resultSetMock.getMetaData()).thenReturn(md);
        Mockito.when(md.getColumnCount()).thenReturn(2);
        Mockito.when(md.getColumnName(1)).thenReturn(columnName1);

        when(resultSetMock.getObject(1)).thenReturn(columnValue);
        when(resultSetMock.next()).thenReturn(true).thenReturn(false);

        List<HashMap<String, Object>> data = metadataService.getDataPreview(tableName1);
        assertEquals(1, data.size());
        assertEquals(columnValue, data.get(0).get(columnName1));
    }

    @Configuration
    @PropertySource("classpath:/test.properties")
    @ComponentScan(basePackages = {"cz.patrik.dbbrowser.service"})
    public static class TestConfig {

        @MockBean
        public ConnectionRepository connectionRepository;
    }
}

