package cz.patrik.dbbrowser;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.stereotype.Component;

import java.sql.Connection;

@Component
public class MockDataSource extends AbstractRoutingDataSource {
    private Connection connection;

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public Connection getConnection() {
        return connection;
    }

    public Connection getConnection(String username, String password) {
        return connection;
    }

    public void afterPropertiesSet() {}

    @Override
    protected Object determineCurrentLookupKey() {
        return null;
    }

}
