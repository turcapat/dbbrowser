package cz.patrik.dbbrowser;

import cz.patrik.dbbrowser.repository.ConnectionRepository;
import cz.patrik.dbbrowser.service.ConnectionManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;



public class ConnectionManagerTest {

    private ConnectionManager connectionManager = new ConnectionManager();

    Map<String, Object> connections = new HashMap<String, Object>();
    ThreadLocal<String> currentConnection = new ThreadLocal<>();

    @Test
    public void setCurrentConnection() {
        cz.patrik.dbbrowser.model.Connection c = new cz.patrik.dbbrowser.model.Connection();
        c.setName("Con Name");
        c.setDatabaseName("DB1");

        connections.put(c.getName(), c);
        ReflectionTestUtils.setField(connectionManager, "dataSourceConnections", connections);
        ReflectionTestUtils.setField(connectionManager, "currentConnection", currentConnection);

        connectionManager.setCurrentConnection(c.getName());
        assertEquals(c.getName(), currentConnection.get());
    }

    @Test
    public void getCurrentConnection() throws SQLException {
        String connectionName = "connectionName";
        String catalogName = "catalogName";

        Connection connection = mock(Connection.class);
        when(connection.getCatalog()).thenReturn(catalogName);
        MockDataSource ds = new MockDataSource();
        ds.setConnection(connection);

        connections.put(connectionName, ds);
        currentConnection.set(connectionName);
        ReflectionTestUtils.setField(connectionManager, "dataSourceConnections", connections);
        ReflectionTestUtils.setField(connectionManager, "currentConnection", currentConnection);

        Connection actualConnection = connectionManager.getActualConnection();
        assertEquals(catalogName, actualConnection.getCatalog());
    }

    @Test
    public void removeConnection() {
        String connectionName = "connectionName";
        MockDataSource ds = new MockDataSource();
        connections.put(connectionName, ds);
        ReflectionTestUtils.setField(connectionManager, "dataSourceConnections", connections);
        ReflectionTestUtils.setField(connectionManager, "abstractRoutingDataSource", new MockDataSource());

        connectionManager.removeDataSource(connectionName);
        assertEquals(0, connections.size());
    }
}
