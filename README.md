# DB Browser

### Assignment
This task is about implementing of web based database browser (similar to desktop
application DBeaver) with basic functionality and for single database vendor only. Browser
should be able to register multiple database connections and browse their data and structure.
...

### Setup
Before start, please set application.properties - mainly default data source.

#### Properties:
*  *data.preview.count=2* - size of data preview for each table

Before running the application, please create database with script in \src\main\resources\db\init.sql (in production I would use Flyway) and 
grant rights for your DB user.

Now run the application. 
You can perform CRUD operation on Collections. (*/api/connection*)
If you create a new connection, it will become available immediately. 
There are two sql examples in resources which helps
you to create new database (*dbbrowserA.sql and dbbrowserB.sql*).

If you have created at least one connection, you can access Metadata API (*/api/metadata*).
Every request to this API must contain header X-Connection-Name.

### Run
mvn clean spring-boot:run

#### Things to improve:
*  Encrypt passwords - use OAuth
*  Flyway for SQL init scripts - actually scripts will not run automatically, you have to run them by yourself.
*  Custom exceptions
*  Create java client to simplify integration 
 